from django.contrib.auth.models import User
from rest_framework import serializers

from app.users.models import UserProfile
from app.users.services import UserService


class UserProfileSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    email = serializers.EmailField(source='user.email')

    class Meta:
        model = UserProfile
        fields = ("id", "first_name", "last_name", "email", "gender")


class UserCreateSerializer(serializers.ModelSerializer):
    gender = serializers.CharField(max_length=1)
    service = UserService()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'gender')

    def create(self, validated_data):
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        return self.service.create(
            username='{}_{}'.format(first_name, last_name),
            first_name=first_name,
            last_name=last_name,
            email=validated_data['email'],
            gender=validated_data['gender'])
