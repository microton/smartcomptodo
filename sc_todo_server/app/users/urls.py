from django.conf.urls import url

from app.users import views

urlpatterns = [
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<user_id>[^/]+)/$', views.UserDetail.as_view()),
]