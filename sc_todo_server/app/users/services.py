from django.contrib.auth.models import User
from django.db.models import Q

from app.users.models import UserProfile


class UserService():

    def __init__(self):
        pass

    def create(self, username, first_name, last_name, email, gender):
        user = User(username=username, first_name=first_name, last_name=last_name, email=email)
        user.save()
        user_profile = UserProfile.objects.create(user=user, gender=gender)
        user_profile.save()
        return user_profile

    def search(self, query):
        queryset = UserProfile.objects.select_related('user').filter(Q(user__first_name__icontains=query) | Q(user__last_name__icontains=query))
        return queryset
