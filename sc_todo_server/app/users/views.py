from rest_framework import status, viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from app.users.models import UserProfile
from app.users.serializers import UserCreateSerializer, UserProfileSerializer
from app.users.services import UserService


class UserViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    service = UserService()

    @list_route(url_path='search')
    def search_users(self, request):
        q = request.GET.get('q')
        matched_users = self.service.search(q);
        serializer = self.get_serializer(matched_users, many=True)
        return Response(serializer.data)

    def create(self, request, format=None):
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid():
            user_profile = serializer.save()
            return Response(UserProfileSerializer(user_profile).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
