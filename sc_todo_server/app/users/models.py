import uuid

from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):

    MALE = 'M'
    FEMALE = 'F'
    GENDER = (
        (MALE, 'MALE'),
        (FEMALE, 'FEMALE')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    gender = models.CharField(max_length=1, choices=GENDER)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_profile'



