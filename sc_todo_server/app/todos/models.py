import uuid

from django.db import models


class Todo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    name = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    deadline_date = models.DateTimeField(blank=True, null=True)
    assigned_to = models.ForeignKey('users.UserProfile', db_column='assigned_to', related_name='assigned_to', on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = 'todo'
        ordering = ('deadline_date',)



