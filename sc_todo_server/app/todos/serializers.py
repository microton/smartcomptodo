from rest_framework import serializers

from app.todos.models import Todo
from app.users.serializers import UserProfileSerializer


class TodoSerializer(serializers.ModelSerializer):
    assigned_to = UserProfileSerializer()

    class Meta:
        model = Todo
        fields = ('id', 'name', 'deadline_date', 'assigned_to')


class TodoCreateUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todo
        fields = ('name', 'deadline_date', 'assigned_to')
