from django.utils.dateparse import parse_date

from app.todos.models import Todo
from app.todos.serializers import TodoSerializer


class TodoService():

    def search_todos(self, search_params):
        todos = Todo.objects.all()

        if 'assignee' in search_params and search_params['assignee']:
            todos = todos.filter(assigned_to=search_params['assignee'])

        if 'deadline_min' in search_params:
            todos = todos.filter(deadline_date__gte=search_params['deadline_min'])

        if 'deadline_max' in search_params:
            todos = todos.filter(deadline_date__lte=search_params['deadline_max'])

        todos = todos.select_related('assigned_to').select_related("assigned_to__user")
        serializer = TodoSerializer(todos, many=True)
        return serializer.data
