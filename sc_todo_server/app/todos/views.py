from datetime import timedelta

from django.utils.dateparse import parse_datetime
from rest_framework import viewsets, status
from rest_framework.decorators import list_route
from rest_framework.response import Response

from app.todos.models import Todo
from app.todos.serializers import TodoSerializer, TodoCreateUpdateSerializer
from app.todos.services import TodoService


class TodoViewSet(viewsets.ModelViewSet):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    lookup_interval = timedelta(days=7)
    service = TodoService()

    @list_route(url_path='search')
    def search_todos(self, request):
        search_params = self.__map_search_params(request)
        return Response(self.service.search_todos(search_params))

    @list_route(url_path='my-todos')
    def get_my_todos(self, request, pk=None):
        pass

    def create(self, request, format=None):
        serializer = TodoCreateUpdateSerializer(data=request.data)
        return self.__save(serializer)

    def update(self, request, pk=None):
        todo = self.get_object();
        serializer = TodoCreateUpdateSerializer(todo, data=request.data)
        return self.__save(serializer)

    def __map_search_params(self, request):
        search_params = {'assignee': request.GET.get('assignee')}
        deadline_param = request.GET.get('deadline')
        if deadline_param:
            search_params['deadline_min'] = parse_datetime(deadline_param)
            search_params['deadline_max'] = search_params['deadline_min'] + self.lookup_interval
        return search_params

    def __save(self, serializer):
        if serializer.is_valid():
            todo = serializer.save()
            return Response(TodoSerializer(todo).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
