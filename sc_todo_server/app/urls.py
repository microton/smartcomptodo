from django.conf.urls import url, include
from rest_framework import routers

from app.todos.views import TodoViewSet

from app.users.views import UserViewSet


from django.views.generic import RedirectView
from django.contrib.staticfiles.views import serve

router = routers.DefaultRouter()
router.register(r'todos', TodoViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [

    url(r'^$', serve,kwargs={'path': 'index.html'}),
    url(r'^(?!/?static/)(?!/?media/)(?P<path>.*\..*)$', RedirectView.as_view(url='/static/%(path)s', permanent=False)),
    url(r'^api/', include(router.urls)),

]