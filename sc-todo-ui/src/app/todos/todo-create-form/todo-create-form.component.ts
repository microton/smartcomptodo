import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  EventEmitter
} from "@angular/core";
import { NgForm, FormBuilder, Validators, AbstractControl, ValidatorFn } from "@angular/forms";
import { ModalDirective } from "ngx-bootstrap";
import { Todo } from "../shared/todo.model";
import { TodosService } from "../shared/todos.service";
import { User } from "../../users/shared/user.model";

import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { of } from "rxjs/observable/of";

import { debounceTime, distinctUntilChanged, switchMap, map } from "rxjs/operators";
import { UsersService } from "../../users/shared/users.service";
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

@Component({
  selector: "app-todo-create-form",
  templateUrl: "./todo-create-form.component.html",
  styleUrls: ["./todo-create-form.component.scss"]
})
export class TodoCreateFormComponent implements OnInit {
  @Output() created = new EventEmitter();
  @Output() closed = new EventEmitter();

  @ViewChild("todoForm") todoForm: NgForm;

  assignee: User;

  form = this.formBuilder.group({
    name: ["", Validators.required],
    assignee: ["", Validators.required],
    deadline: ["", Validators.required]
  });

  constructor(
    private todosService: TodosService,
    private usersService: UsersService,
    private formBuilder: FormBuilder
  ) { }

  users: Observable<User[]>;
  private searchTerms = new Subject<string>();

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {

    this.users = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) =>
        this.usersService.search(term)
          .pipe(map(users => {
            return users.map(u => ({ ...u, name: u.first_name + ' ' + u.last_name }))
          }))
      ),
    );

  }

  submit() {

    const { name, deadline: deadline_date } = this.form.value;
    const assigned_to = this.assignee.id;

    this.todosService
      .createTodo({ name, deadline_date, assigned_to })
      .subscribe(this.onCreateSuccess, this.onCreateError);
  }

  private onCreateSuccess = () => {
    this.created.emit();
  };

  private onCreateError = () => {
    this.created.emit();
  };

  close() {
    this.closed.emit();
  }

  onAssigneeSelect(event: TypeaheadMatch): void {
    this.assignee = event.item;
  }

  onAssigneeChange(value: string) {
    this.assignee = null;
    this.form.get('assignee').setErrors({ 'incorrect': true });
  }
}
