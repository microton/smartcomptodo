import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  EventEmitter
} from '@angular/core';
import { NgForm, FormBuilder, Validators } from "@angular/forms";
import { ModalDirective } from 'ngx-bootstrap';
import { Todo } from '../shared/todo.model';
import { TodosService } from '../shared/todos.service';
import { ActivatedRoute } from '@angular/router';

import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { of } from "rxjs/observable/of";

import { debounceTime, distinctUntilChanged, switchMap, map } from "rxjs/operators";
import { UsersService } from "../../users/shared/users.service";
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { User } from '../../users/shared/user.model';

@Component({
  selector: 'app-todo-edit-form',
  templateUrl: './todo-edit-form.component.html',
  styleUrls: ['./todo-edit-form.component.scss']
})
export class TodoEditFormComponent implements OnInit {
  @Output() edited = new EventEmitter();
  @Output() closed = new EventEmitter();

  @ViewChild('todoForm') todoForm: NgForm;

  todoId: number;
  loading = true;
  form: any;
  assignee: User;
  users: Observable<User[]>;
  private searchTerms = new Subject<string>();

  constructor(
    private todosService: TodosService,
    private usersService: UsersService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) { }

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.todoId = params['todoId'];
      this.todosService.getTodo(this.todoId).subscribe(todo => {
        this.form = this.formBuilder.group({
          name: [todo.name, Validators.required],
          assignee: [todo.assigned_to.first_name + ' ' + todo.assigned_to.last_name, Validators.required],
          deadline: [todo.deadline_date, Validators.required]
        });
        this.assignee = todo.assigned_to;
        this.loading = false;
      });
    });

    this.users = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) =>
        this.usersService.search(term)
          .pipe(map(users => {
            return users.map(u => ({ ...u, name: u.first_name + ' ' + u.last_name }))
          }))
      ),
    );
  }

  submit() {

    const { name, deadline: deadline_date } = this.form.value;
    const assigned_to = this.assignee.id;

    this.todosService
      .updateTodo(this.todoId, { name, deadline_date, assigned_to })
      .subscribe(this.onEditSuccess, this.onEditError);
  }

  private onEditSuccess = () => {
    this.edited.emit();
  }

  private onEditError = () => {
    this.edited.emit();
  }

  close() {
    this.closed.emit();
  }

  onAssigneeSelect(event: TypeaheadMatch): void {
    this.assignee = event.item;
  }

  onAssigneeChange(value: string) {
    this.assignee = null;
    this.form.get('assignee').setErrors({ 'incorrect': true });
  }
}
