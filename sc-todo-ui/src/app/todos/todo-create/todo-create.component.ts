import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-create',
  templateUrl: './todo-create.component.html',
  styleUrls: ['./todo-create.component.scss']
})
export class TodoCreateComponent implements OnInit, AfterViewInit {
  @ViewChild('todoModal') todoModal: ModalDirective;

  constructor(private router: Router) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.show();

    this.todoModal.onHide.subscribe(() => {
      this.router.navigate(['/todos']);
    });
  }

  show() {
    this.todoModal.show();
  }

  hide() {
    this.todoModal.hide();
  }
}
