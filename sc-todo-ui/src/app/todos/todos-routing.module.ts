import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodosComponent } from './todos.component';
import { TodoCreateComponent } from './todo-create/todo-create.component';
import { TodoEditComponent } from './todo-edit/todo-edit.component';
import { TodoDeleteComponent } from './todo-delete/todo-delete.component';

const routes: Routes = [
  {
    path: 'todos',
    component: TodosComponent,
    children: [
      {
        path: 'create',
        component: TodoCreateComponent
      },
      {
        path: 'edit/:todoId',
        component: TodoEditComponent
      },
      {
        path: 'delete/:todoId',
        component: TodoDeleteComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TodosRoutingModule { }
