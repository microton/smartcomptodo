import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TodosService } from './shared/todos.service';
import { Todo } from './shared/todo.model';
import { NgForm, FormBuilder, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { of } from "rxjs/observable/of";

import { debounceTime, distinctUntilChanged, switchMap, map } from "rxjs/operators";

import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { User } from '../users/shared/user.model';
import { UsersService } from '../users/shared/users.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  todos: Todo[];
  users: Observable<User[]>;

  assignee: string = null;
  deadline: string = null;

  private searchTerms = new Subject<string>();

  constructor(private router: Router, private todosService: TodosService, private usersService: UsersService, private formBuilder: FormBuilder) { }

  form = this.formBuilder.group({
    assignee: ["", Validators.required],
    deadline: ["", Validators.required]
  });

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {

    this.todosService.getTodos().subscribe(todos => this.todos = todos);

    this.users = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) =>
        this.usersService.search(term)
          .pipe(map(users => {
            return users.map(u => ({ ...u, name: u.first_name + ' ' + u.last_name }))
          }))
      ),
    );

  }

  createTodo() {
    this.router.navigate(['todos/create']);
  }

  editTodo(id: number) {
    this.router.navigate(['todos/edit/' + id]);
  }

  deleteTodo(id: number) {
    this.router.navigate(['todos/delete/' + id]);
  }

  refresh($event) {
    this.todosService.searchTodos(this.assignee, this.deadline).subscribe(todos => this.todos = todos);
  }

  onAssigneeSelect(event: TypeaheadMatch): void {
    this.assignee = event.item.id;
    this.refresh(null);
  }

  onAssigneeChange(searchValue: string) {

    let shouldRefresh = this.assignee != null;
    this.assignee = null;
    if (shouldRefresh) {
      this.refresh(null);
    }

    this.form.get('assignee').setErrors({ 'incorrect': true });
  }

  onDeadlineChange(value: Date) {

    let tmpDeadline;

    if (value != null && Object.prototype.toString.call(value) === "[object Date]" && !isNaN(value.getTime())) {
      tmpDeadline = value.toISOString();
    } else {
      tmpDeadline = null;
    }

    if (this.deadline != tmpDeadline) {
      this.deadline = tmpDeadline;
      this.refresh(null);
    }
  }

}
