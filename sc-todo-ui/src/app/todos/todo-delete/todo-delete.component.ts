import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-delete',
  templateUrl: './todo-delete.component.html',
  styleUrls: ['./todo-delete.component.scss']
})
export class TodoDeleteComponent implements AfterViewInit {

  @ViewChild('todoModal') todoModal: ModalDirective;

  constructor(private router: Router) { }

  ngAfterViewInit() {
    this.show();

    this.todoModal.onHide.subscribe(() => {
      this.router.navigate(['/todos']);
    });
  }

  show() {
    this.todoModal.show();
  }

  hide() {
    this.todoModal.hide();
  }
}
