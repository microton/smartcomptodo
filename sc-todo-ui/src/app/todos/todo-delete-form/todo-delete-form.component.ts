import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  EventEmitter
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { Todo } from '../shared/todo.model';
import { TodosService } from '../shared/todos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todo-delete-form',
  templateUrl: './todo-delete-form.component.html',
  styleUrls: ['./todo-delete-form.component.scss']
})
export class TodoDeleteFormComponent implements OnInit {
  @Output() deleted = new EventEmitter();
  @Output() closed = new EventEmitter();

  @ViewChild('todoForm') todoForm: NgForm;

  todoId: number;

  constructor(
    private todosService: TodosService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.todoId = params['todoId'];
    });
  }

  submit() {
    this.todosService
      .deleteTodo(this.todoId)
      .subscribe(this.onDeleteSuccess, this.onDeleteError);
  }

  private onDeleteSuccess = () => {
    this.deleted.emit();
  }

  private onDeleteError = () => {
    this.deleted.emit();
  }

  close() {
    this.closed.emit();
  }
}
