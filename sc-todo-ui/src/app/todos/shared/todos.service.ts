import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/timer';
import { Todo } from './todo.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TodosService {

  private todosUrl = 'api/todos';

  constructor(private http: HttpClient) { }

  createTodo(todo: { name: string; deadline_date: string; assigned_to: string }) {
    const url = `${this.todosUrl}/`;
    return this.http.post(url, todo, httpOptions).catch(this.handleError);
  }

  updateTodo(id: number, todo: { name: string; deadline_date: string; assigned_to: string }) {
    const url = `${this.todosUrl}/${id}/`;
    return this.http.put(url, todo, httpOptions).catch(this.handleError);
  }

  deleteTodo(id: number) {
    const url = `${this.todosUrl}/${id}/`;
    return this.http.delete(url).catch(this.handleError);
  }

  getTodo(id: number): Observable<Todo> {
    const url = `${this.todosUrl}/${id}/`;
    return this.http.get<Todo[]>(url).catch(this.handleError);
  }

  getTodos(): Observable<Todo[]> {
    const url = `${this.todosUrl}/`;
    return this.http.get<Todo[]>(url).catch(this.handleError);
  }

  searchTodos(assignee: string, deadline: string): Observable<Todo[]> {
    const url = `${this.todosUrl}/search?assignee=${assignee || ''}&deadline=${deadline || ''}`;
    return this.http.get<Todo[]>(url).catch(this.handleError);
  }

  private handleError(error: any) {
    console.error(error);
    return Observable.throw(error);
  }
}
