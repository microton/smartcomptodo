import { User } from '../../users/shared/user.model';

export class Todo {
    id?: number;
    name: string;
    deadline_date: string;
    assigned_to: User;
}
