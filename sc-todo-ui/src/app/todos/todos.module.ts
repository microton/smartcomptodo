import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TodosComponent } from './todos.component';
import { TodosRoutingModule } from './todos-routing.module';
import { TodoCreateComponent } from './todo-create/todo-create.component';
import { TodoCreateFormComponent } from './todo-create-form/todo-create-form.component';
import { TodosService } from './shared/todos.service';
import { ModalModule, BsDatepickerModule, TypeaheadModule } from 'ngx-bootstrap';
import { TodoEditComponent } from './todo-edit/todo-edit.component';
import { TodoEditFormComponent } from './todo-edit-form/todo-edit-form.component';
import { TodoDeleteComponent } from './todo-delete/todo-delete.component';
import { TodoDeleteFormComponent } from './todo-delete-form/todo-delete-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    BsDatepickerModule,
    ReactiveFormsModule,
    TypeaheadModule,
    TodosRoutingModule
  ],
  declarations: [
    TodosComponent,
    TodoCreateComponent,
    TodoCreateFormComponent,
    TodoEditComponent,
    TodoEditFormComponent,
    TodoDeleteComponent,
    TodoDeleteFormComponent
  ],
  providers: [
    TodosService
  ]
})
export class TodosModule { }
