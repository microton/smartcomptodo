import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.scss']
})
export class TodoEditComponent implements AfterViewInit {
  @ViewChild('todoModal') todoModal: ModalDirective;

  constructor(private router: Router) { }

  ngAfterViewInit() {
    this.show();

    this.todoModal.onHide.subscribe(() => {
      this.router.navigate(['/todos']);
    });
  }

  show() {
    this.todoModal.show();
  }

  hide() {
    this.todoModal.hide();
  }
}
