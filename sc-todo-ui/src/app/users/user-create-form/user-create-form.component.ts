import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { NgForm, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';
import { User } from '../shared/user.model';
import { UsersService } from '../shared/users.service';

@Component({
  selector: 'app-user-create-form',
  templateUrl: './user-create-form.component.html',
  styleUrls: ['./user-create-form.component.scss']
})
export class UserCreateFormComponent {

  @Output() created = new EventEmitter();
  @Output() closed = new EventEmitter();

  @ViewChild('userForm') userForm: NgForm;

  user: User = new User();

  form = this.formBuilder.group({
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    email: ['', Validators.required],
    gender: ['', Validators.required],
  });

  constructor(private usersService: UsersService, private formBuilder: FormBuilder) { }

  submit() {
    this.usersService.createUser({
      ...this.user,
      ...this.form.value,
    }).subscribe(this.onCreateSuccess, this.onCreateError);
  }

  private onCreateSuccess = () => {
    this.created.emit();
  }

  private onCreateError = () => {
    this.created.emit();
  }

  close() {
    this.closed.emit();
  }


}
