import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserCreateFormComponent } from './user-create-form/user-create-form.component';
import { UsersService } from './shared/users.service';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    SharedModule,
    ModalModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    ButtonsModule,
  ],
  declarations: [
    UsersComponent,
    UserCreateComponent,
    UserCreateFormComponent
  ],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
