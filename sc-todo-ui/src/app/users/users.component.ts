import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { UserCreateComponent } from './user-create/user-create.component';
import { Router } from '@angular/router';
import { UsersService } from './shared/users.service';
import { User } from './shared/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @ViewChild('userCreate') userCreate: UserCreateComponent;

  users: User[];

  constructor(private router: Router, private usersService: UsersService) { }

  ngOnInit() {
    this.usersService.getUsers().subscribe(users => this.users = users);
  }

  createUser() {
    this.router.navigate(['users/create']);
  }

  refresh($event) {
    this.usersService.getUsers().subscribe(users => this.users = users);
  }

}
