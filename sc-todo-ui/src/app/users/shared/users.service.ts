import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/timer';
import { User } from './user.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UsersService {

  private usersUrl = 'api/users';

  constructor(private http: HttpClient) { }

  createUser(user: User) {
    const url = `${this.usersUrl}/`;
    return this.http.post(url, user, httpOptions).catch(this.handleError);
  }

  getUsers(): Observable<User[]> {
    const url = `${this.usersUrl}/`;
    return this.http.get<User[]>(url).catch(this.handleError);
  }

  search(query: string): Observable<User[]> {
    const url = `${this.usersUrl}/search?q=${query}`;
    return this.http.get<User[]>(url).catch(this.handleError);
  }

  private handleError(error: any) {
    console.error(error);
    return Observable.throw(error);
  }
}
