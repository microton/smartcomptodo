import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit, AfterViewInit {

  @ViewChild('userModal') userModal: ModalDirective;

  constructor(private router: Router) { }

  ngOnInit() {
    console.log('UserCreateComponent ngOnInit');
  }

  ngAfterViewInit() {

    this.show();

    this.userModal.onHide.subscribe(() => {
      this.router.navigate(['/users']);
    });
  }

  show() {
    this.userModal.show();
  }

  hide() {
    this.userModal.hide();
  }

}
